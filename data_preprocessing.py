# %% md

# A lot of this code is from https://towardsdatascience.com/how-to-use-torchtext-for-neural-machine-translation-plus-hack-to-make-it-5x-faster-77f3884d95

# %%
import math

import torch
from torchtext import data
from torchtext.data import Field, TabularDataset
import pandas as pd
from sklearn.model_selection import train_test_split

# DATA_FILE = 'python_lines.txt'
# from project.code_autocomplete.TransformerModel import TransformerModel

DATA_FILE = 'python_lines_subset.txt'

# %%

# engine=Python prevents buffer overflow
print("Reading data in")
df = pd.read_csv(DATA_FILE, sep='\n', header=0, names=["code"], skipinitialspace=True, error_bad_lines=False,
                 warn_bad_lines=False, engine='python')
print("Finished reading in data")

# %%

# create train and validation set
train, val = train_test_split(df, test_size=0.1)
train.to_csv("train.csv", index=False)
val.to_csv("val.csv", index=False)

#%%

def data_generator(file_name):
    for row in open('train.csv', 'r'):
        for i in range(len(row)):
            yield row[:i], row[i]

#%%
next_data = data_generator('train.csv')
#%%
print(next(next_data))

#%%

# Tokenize using list bc we want each char to be different
TEXT = Field(tokenize=list)

data_fields = [('code', TEXT)]

# %%

train, val = TabularDataset.splits(path='./', train='train.csv', validation='val.csv', format='csv', fields=data_fields)

# %%

TEXT.build_vocab(train, val)

# %%

# train_iter = BucketIterator(train, batch_size=20, sort_key = lambda x: len(x.code), shuffle=True)

# %%

# batch = next(iter(train_iter))
# print(batch.code)

# %%
#
# # code originally from http://nlp.seas.harvard.edu/2018/04/03/attention.html but editted
# global max_src_in_batch, max_tgt_in_batch
#
#
# def batch_size_fn(new, count, sofar):
#     "Keep augmenting batch and calculate total number of tokens + padding."
#     global max_src_in_batch, max_tgt_in_batch
#     if count == 1:
#         max_src_in_batch = 0
#     max_src_in_batch = max(max_src_in_batch, len(new.code))
#     src_elements = count * max_src_in_batch
#     return src_elements
#
#
# class MyIterator(data.Iterator):
#     def create_batches(self):
#         if self.train:
#             def pool(d, random_shuffler):
#                 for p in data.batch(d, self.batch_size * 100):
#                     p_batch = data.batch(
#                         sorted(p, key=self.sort_key),
#                         self.batch_size, self.batch_size_fn)
#                     for b in random_shuffler(list(p_batch)):
#                         yield b
#
#             self.batches = pool(self.data(), self.random_shuffler)
#
#         else:
#             self.batches = []
#             for b in data.batch(self.data(), self.batch_size,
#                                 self.batch_size_fn):
#                 self.batches.append(sorted(b, key=self.sort_key))
#
#
# # %%
# device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# print(f'Running on: {device}')
# train_iter = MyIterator(train, batch_size=1300, device=device,
#                         repeat=False, sort_key=lambda x: (len(x.code)),
#                         batch_size_fn=batch_size_fn, train=True,
#                         shuffle=True)
# val_iter = MyIterator(val, batch_size=1300, device=device,
#                       repeat=False, sort_key=lambda x: (len(x.code)),
#                       batch_size_fn=batch_size_fn, train=True,
#                       shuffle=True)

# %%
# batch = next(iter(train_iter))
# print(batch.code)
# print(f'{ len(train_iter.dataset) = }')


# # %%
#
# bptt = 35
#
#
# def get_batch(source, i):
#     seq_len = min(bptt, len(source) - 1 - i)
#     data = source[i:i + seq_len]
#     target = source[i + 1:i + 1 + seq_len].view(-1)
#     return data, target
#
#
#%%

print(train_iter.data()[1].code)
ntokens = len(TEXT.vocab.stoi)  # the size of vocabulary

#%%

from keras.models import Sequential
from keras.layers import LSTM, Dense, Dropout, Masking, Embedding

model = Sequential()

# Embedding layer
model.add(
    Embedding(input_dim=ntokens,
              output_dim=100,
              trainable=True,
              mask_zero=True))

# Masking layer for pre-trained embeddings
# model.add(Masking(mask_value=0.0))

# Recurrent layer
model.add(LSTM(64, return_sequences=False,
               dropout=0.1, recurrent_dropout=0.1))

# Fully connected layer
model.add(Dense(64, activation='relu'))

# Dropout for regularization
model.add(Dropout(0.5))

# Output layer
model.add(Dense(ntokens, activation='softmax'))

# Compile the model
model.compile(
    optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

#%%
from keras.callbacks import EarlyStopping, ModelCheckpoint


# Create callbacks
callbacks = [EarlyStopping(monitor='val_loss', patience=5),
             ModelCheckpoint('../models/model.h5', save_best_only=True,
                             save_weights_only=False)]
#%%

print(f'{train_iter.data()[1].code =}')



#%%
history = model.fit(X_train,  y_train,
                    batch_size=2048, epochs=150,
                    callbacks=callbacks,
                    validation_data=(X_valid, y_valid))


# %%
#
# ntokens = len(TEXT.vocab.stoi)  # the size of vocabulary
# emsize = 200  # embedding dimension
# nhid = 200  # the dimension of the feedforward network model in nn.TransformerEncoder
# nlayers = 2  # the number of nn.TransformerEncoderLayer in nn.TransformerEncoder
# nhead = 2  # the number of heads in the multiheadattention models
# dropout = 0.2  # the dropout value
# model = TransformerModel(ntokens, emsize, nhead, nhid, nlayers, dropout).to(device)
#
# # %%
#
# # %%
# import torch.nn as nn
#
# criterion = nn.CrossEntropyLoss()
# lr = 5.0  # learning rate
# optimizer = torch.optim.SGD(model.parameters(), lr=lr)
# scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 1.0, gamma=0.95)
#
# import time
#
#
# def train():
#     model.train()  # Turn on the train mode
#     total_loss = 0.
#     start_time = time.time()
#     ntokens = len(TEXT.vocab.stoi)
#     for batch, i in enumerate(range(0, train_iter.data().size(0) - 1, bptt)):
#         data, targets = get_batch(train_iter.data(), i)
#         optimizer.zero_grad()
#         output = model(data)
#         loss = criterion(output.view(-1, ntokens), targets)
#         loss.backward()
#         torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)
#         optimizer.step()
#
#         total_loss += loss.item()
#         log_interval = 200
#         if batch % log_interval == 0 and batch > 0:
#             cur_loss = total_loss / log_interval
#             elapsed = time.time() - start_time
#             print('| epoch {:3d} | {:5d}/{:5d} batches | '
#                   'lr {:02.2f} | ms/batch {:5.2f} | '
#                   'loss {:5.2f} | ppl {:8.2f}'.format(
#                 -1, batch, len(train_iter.data()) // bptt, scheduler.get_lr()[0],
#                            elapsed * 1000 / log_interval,
#                 cur_loss, math.exp(cur_loss)))
#             total_loss = 0
#             start_time = time.time()
#
#
# def evaluate(eval_model, data_source):
#     eval_model.eval()  # Turn on the evaluation mode
#     total_loss = 0.
#     ntokens = len(TEXT.vocab.stoi)
#     with torch.no_grad():
#         for i in range(0, data_source.size(0) - 1, bptt):
#             data, targets = get_batch(data_source, i)
#             output = eval_model(data)
#             output_flat = output.view(-1, ntokens)
#             total_loss += len(data) * criterion(output_flat, targets).item()
#     return total_loss / (len(data_source) - 1)
#
#
# # %%
# best_val_loss = float("inf")
# epochs = 3  # The number of epochs
# best_model = None
#
# for epoch in range(1, epochs + 1):
#     epoch_start_time = time.time()
#     train()
#     val_loss = evaluate(model, val)
#     print('-' * 89)
#     print('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.2f} | '
#           'valid ppl {:8.2f}'.format(epoch, (time.time() - epoch_start_time),
#                                      val_loss, math.exp(val_loss)))
#     print('-' * 89)
#
#     if val_loss < best_val_loss:
#         best_val_loss = val_loss
#         best_model = model
#
#     scheduler.step()
#
