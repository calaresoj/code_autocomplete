#%%
import pandas as pd
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import LSTM, Dense, Dropout, Masking, Embedding
from keras.callbacks import EarlyStopping, ModelCheckpoint
import numpy as np
from torchtext.data import Field, TabularDataset


#%%
# DATA_FILE = 'python_lines.txt'
DATA_FILE = 'python_lines_subset.txt'

# %%

# engine=Python prevents buffer overflow
print("Reading data in")
df = pd.read_csv(DATA_FILE, sep='\n', header=0, names=["code"], skipinitialspace=True, error_bad_lines=False,
                 warn_bad_lines=False, engine='python')
print("Finished reading in data")

# %%

# create train and validation set
train, val = train_test_split(df, test_size=0.5, random_state=0)
train.to_csv("train.csv", index=False)
val.to_csv("val.csv", index=False)

#%%

def data_generator(file_name):
    for row in open(file_name, 'r'):
        row = [ord(c) for c in list(row)]
        for i in range(1, len(row)):
            return_tuple = (np.array(row[:i]), np.array(row[i]).reshape(-1, 1))
            print(f'{file_name = }, {return_tuple =}')
            yield return_tuple

#%%

# Tokenize using list bc we want each char to be different
TEXT = Field(tokenize=list)

data_fields = [('code', TEXT)]

# %%

train, val = TabularDataset.splits(path='./', train='train.csv', validation='val.csv', format='csv', fields=data_fields)

# %%

TEXT.build_vocab(train, val)

#%%
# ntokens = len(TEXT.vocab.stoi)  # the size of vocabulary
ntokens = 128
#%%


model = Sequential()

# Embedding layer
model.add(
    Embedding(input_dim=ntokens,
              output_dim=128,
              trainable=True,
              mask_zero=True))

# Masking layer for pre-trained embeddings
# model.add(Masking(mask_value=0.0))

# Recurrent layer
model.add(LSTM(64, return_sequences=False,
               dropout=0.1, recurrent_dropout=0.1))

# Fully connected layer
model.add(Dense(64, activation='relu'))

# Dropout for regularization
model.add(Dropout(0.5))

# Output layer
model.add(Dense(ntokens, activation='softmax'))

# Compile the model
model.compile(
    optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

#%%

# Create callbacks
callbacks = [EarlyStopping(monitor='val_loss', patience=5),
             ModelCheckpoint('../model.h5', save_best_only=True,
                             save_weights_only=False)]

#%%
data_gen = data_generator('train.csv')
data_gen_val = data_generator('val.csv')

#%%

# while f:= next(data_gen):
#     print(f)



#%%

# I have no idea what to put for validation steps, so I just tried 10. Same with steps per epoch
history = model.fit(data_gen,
                    epochs=1,
                    callbacks=callbacks,
                    validation_data=data_gen_val,
                    validation_steps=1,
                    steps_per_epoch=1)
