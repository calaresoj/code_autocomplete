import base64
import requests

url = 'https://api.github.com/repos/JakeBellis/BC3RobotProject/contents/src/GUIGeometry.py' #take a repo using the github api. Using a personal one for testing
req = requests.get(url)

if req.status_code == requests.codes.ALL_OK: #html request for the file
    req = req.json()
    content =  base64.b64decode(req['content'])
else:
    print('Content was not found.')

print(len(content))
textString = content.decode('utf-8')

isComment = False

for i in range(len(textString)): #removes normal comments in python code
    if(i == len(textString) - 1):
        break
    if(textString[i] == '\n'):
        isComment = False
    if(textString[i] == '#'):
        isComment = True
    if(isComment):
        textString = textString[:i] + "" + textString[i + 1:]
        i = i-1
        continue





