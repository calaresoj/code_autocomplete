Welcome to our project! You can train the model by running lstm_no_gen.py and get predictions by running python3 predict_using_model.py <string to predict from>
We have a lot of extra code here from ideas we almost had working, most notably the Transformer and generator fed LSTM.
