
# Load LSTM network and generate text
import sys
import numpy
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils
from sklearn.model_selection import train_test_split
import pandas as pd

# from flask import Flask, url_for


# app = Flask(__name__)


# DATA_FILE = 'python_lines.txt'


# DATA_FILE = 'python_lines_subset.txt'
# Uncomment when first making the datasets in your folder, then there is no need to do this again
# # %%
#
# # engine=Python prevents buffer overflow
# print("Reading data in")
# df = pd.read_csv(DATA_FILE, sep='\n', header=0, names=["code"], skipinitialspace=True, error_bad_lines=False,
#                  warn_bad_lines=False, engine='python')
# print("Finished reading in data")
#
# # %%
#
# # create train and validation set
# train, val = train_test_split(df, test_size=0.1, random_state=0)
# train.to_csv("train_small.txt", index=False, sep='\n')
# val.to_csv("val_small.txt", index=False, sep='\n')


def read_in_data():
    """
    Not very pretty with all the things beign yielded, can clean up a lot
    """
    # load ascii text and covert to lowercase
    filename = "train.csv"
    raw_text = open(filename, 'r', encoding='utf-8').read()
    # create mapping of unique chars to integers, and a reverse mapping
    chars = sorted(list(set(raw_text)))
    char_to_int = dict((c, i) for i, c in enumerate(chars))
    int_to_char = dict((i, c) for i, c in enumerate(chars))
    # summarize the loaded data
    n_chars = len(raw_text)
    n_vocab = len(chars)
    print("Total Characters: ", n_chars)
    print("Total Vocab: ", n_vocab)
    return raw_text, char_to_int, int_to_char, n_chars, n_vocab


def get_model(length):
    # prepare the dataset of input to output pairs encoded as integers
    seq_length = length
    dataX = []
    dataY = []
    for i in range(0, n_chars - seq_length, 1):
        seq_in = raw_text[i:i + seq_length]
        seq_out = raw_text[i + seq_length]
        dataX.append([char_to_int[char] for char in seq_in])
        dataY.append(char_to_int[seq_out])
    n_patterns = len(dataX)
    print("Total Patterns: ", n_patterns)
    # reshape X to be [samples, time steps, features]
    X = numpy.reshape(dataX, (n_patterns, seq_length, 1))
    # normalize
    X = X / float(n_vocab)
    # one hot encode the output variable
    y = np_utils.to_categorical(dataY)
    # define the LSTM model
    model = Sequential()
    model.add(LSTM(256, input_shape=(X.shape[1], X.shape[2])))
    model.add(Dropout(0.2))
    model.add(Dense(y.shape[1], activation='softmax'))
    return model


def load_weights(model, weights_file):
    # load the network weights
    # filename = "weights-improvement-20-3.0209.hdf5"
    # filename = "weights-improvement-03-2.4804.hdf5"
    filename = weights_file
    model.load_weights(filename)
    model.compile(loss='categorical_crossentropy', optimizer='adam')


def predict(string, response_length, model):
    # pick a random seed
    # start = numpy.random.randint(0, len(dataX) - 1)
    #
    #
    # pattern = dataX[start]
    test_string = string
    pattern = [char_to_int[c] for c in test_string]

    print(f"Seed: {pattern}")
    return_string_array = []  # Array so we don't keep recopying strings
    # print("\"", ''.join([int_to_char[value] for value in pattern]), "\"")
    # generate characters
    for i in range(response_length):
        x = numpy.reshape(pattern, (1, len(pattern), 1))
        x = x / float(n_vocab)
        prediction = model.predict(x, verbose=0)
        # print(f"prediction: {prediction}")
        index = numpy.argmax(prediction)
        result = int_to_char[index]
        seq_in = [int_to_char[value] for value in pattern]
        sys.stdout.write(result)
        return_string_array.append(result)
        pattern.append(index)
        pattern = pattern[1:len(pattern)]
    return "".join(return_string_array)


raw_text, char_to_int, int_to_char, n_chars, n_vocab = read_in_data()

# app = Flask(__name__)


response_length = 10


# @app.route('/<string:typed_so_far>', methods=['GET'])
# @app.route('/<string:typed_so_far>', methods=['GET'])
def handle_api_call(typed_so_far):
    print(f'Handling API, typed_so_far: {typed_so_far}')
    model = get_model(len(typed_so_far))
    load_weights(model, "weights-improvement-03-2.4804.hdf5")
    predicted = predict(typed_so_far, response_length, model)
    return predicted


def main():
    if len(sys.argv) != 2:
        print("Usage: python3 predict_using_model.py <string so far>")
        exit(1)
    else:
        typed_so_far = sys.argv[1]
        return handle_api_call(typed_so_far)
# print(handle_api_call("testin"))

if __name__ == '__main__':
    main()