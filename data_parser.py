
# For reading directly into Python
# data = [""]
# with open("subset.txt", "r", encoding="utf-8") as input_file:
#     while char := input_file.read(1):
#         if data[len(data) - 1].endswith('\\n'):
#             data[len(data) - 1] = data[len(data) - 1][:-2]
#             data.append(char)
#         else:
#             data[len(data) - 1] = data[len(data) - 1] + char
# for item in data:
#     print(item)

# current_line = ""
# with open("raw_data.txt", "r", encoding="utf-8") as input_file, open("clean_data.txt", "w", encoding="utf-8") as output_file:
#     while char := input_file.read(1):
#         if current_line.endswith("\\") and char == 'n':
#             current_line = current_line[:-1]
#             output_file.write(current_line)
#             output_file.write("\n")
#             current_line = ""
#         else:
#             current_line = current_line + char
#
# print("Done!")

import sqlite3
with sqlite3.connect('code_and_comments/all_data.db') as original_db_conn, sqlite3.connect('python_data.db') as py_conn:
    original_db_cursor = original_db_conn.cursor()
    original_db_cursor.execute('SELECT * FROM all_data;')

    py_cursor = py_conn.cursor()
    py_cursor.execute("DROP TABLE IF EXISTS py_code;")
    py_cursor.execute('''CREATE TABLE py_code (code text);''')

    while row := original_db_cursor.fetchone():
        # We only care about Python files
        # The 0 index in row is the filename, the 1 index is the text, the 2 index is the comment associated with
        # it, which we don't care about (its just a relic of where we got the data from)
        if row[0].endswith('.py'):
            code = row[1]
            current_line = ""

            # I am assuming we will ignore connections between lines and will just save all lines into the db seperatly.
            # This should be trivial to change in the future if we need to
            for char in code:
                if char == '\n':
                    if current_line.strip() != "":
                        # print(f'Row: {current_line}')
                        py_cursor.execute('INSERT INTO py_code (code) VALUES (?);', (current_line,))
                        current_line = ""
                else:
                    current_line = current_line + char
    py_conn.commit()
