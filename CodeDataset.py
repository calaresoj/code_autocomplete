from torch.utils.data import Dataset


class CodeDataset(Dataset):
    """
    A dataset of python code. A lot of help from https://towardsdatascience.com/building-efficient-custom-datasets-in-pytorch-2563b946fd9f
    args:
        source: A path to a file with each line of code on a new line, either relative or absolute or an array of samples
    """

    def __init__(self, source, max_size=-1):

        # Yeah, this isn't great style, might change later. I want to be able
        # to reuse the constructor though in split w/o rereading everything in
        if type(source) == str:
            self.samples = self._import_data(source, max_size)
        else:
            self.samples = source

    @staticmethod
    def _import_data(source, max_size=-1):
        samples = []
        with open(source) as file:
            index = 0
            while line := file.readline():
                if max_size == -1 or max_size > index:
                    samples.append(line.strip())
                    index = index + 1
                else:
                    break
        return samples

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        return self.samples[idx]

    def splits(self, amount_in_first_return_value):
        """
        Splits the data into two datasets and returns them as a tuple.
        The first one returned if the given fraction of the total data
        :param amount_in_first_return_value:
        :return: Tuple
        """
        index_to_split_on = int(amount_in_first_return_value * len(self))
        return CodeDataset(self[0:index_to_split_on]), CodeDataset(self[index_to_split_on:])

if __name__ == '__main__':
    dataset = CodeDataset('python_lines_subset.txt', max_size=10)
    print(len(dataset))
    print(dataset.splits(0.6))
    print(dataset[2:5])
