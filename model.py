import string

import spacy
import torch
from torch import nn
from torch.nn import Transformer
from torchtext.data import ReversibleField
import torchtext

# Split each line into chars and send to Field
from torchtext.datasets import LanguageModelingDataset

from project.code_autocomplete.CodeDataset import CodeDataset

TEXT = ReversibleField(sequential=True, tokenize=list)

datafields = [('code', TEXT)]

# training_data, testing_data = TabularDataset.splits(path='python_lines_subset.txt', format='csv',
#                                              fields=datafields, skip_header=False,
#                                              csv_reader_params={"delimiter": "\n"})
# data = CodeDataset('python_lines_subset.txt')

dataset = LanguageModelingDataset('python_lines_subset.txt', TEXT)
print(dataset[0].text)

# training_data, testing_data = data.splits()

TEXT.build_vocab(string.printable)
print(dataset[0].text)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

transformer_model = nn.Transformer(nhead=16, num_encoder_layers=12)

out = transformer_model(dataset, dataset)

#
# # From https://pytorch.org/tutorials/beginner/transformer_tutorial.html
# def batchify(data, bsz):
#     data = TEXT.numericalize([data.examples[0].text])
#     # Divide the dataset into bsz parts.
#     nbatch = data.size(0) // bsz
#     # Trim off any extra elements that wouldn't cleanly fit (remainders).
#     data = data.narrow(0, 0, nbatch * bsz)
#     # Evenly divide the data across the bsz batches.
#     data = data.view(bsz, -1).t().contiguous()
#     return data.to(device)
#
# batch_size = 4
# train_data = batchify(dataset, batch_size)
