# Code taken largely from https://machinelearningmastery.com/text-generation-lstm-recurrent-neural-networks-python-keras/

# Small LSTM Network to Generate Text for Alice in Wonderland
import numpy
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils


# %%
# DATA_FILE = 'python_lines.txt'
from sklearn.model_selection import train_test_split

DATA_FILE = 'python_lines_larger_subset.txt'

# %%

# engine=Python prevents buffer overflow
# print("Reading data in")
# df = pd.read_csv(DATA_FILE, sep='\n', header=0, names=["code"], skipinitialspace=True, error_bad_lines=False,
#                  warn_bad_lines=False, engine='python')
# print("Finished reading in data")
#
# # %%
#
# # create train and validation set
# train, val = train_test_split(df, test_size=0.1, random_state=0)
# train.to_csv("train.csv", index=False, sep='\n')
# val.to_csv("val.csv", index=False, sep='\n')

# %%

filename = "train.csv"
raw_text = open(filename, 'r', encoding='utf-8').read()

# create mapping of unique chars to integers
chars = sorted(list(set(raw_text)))
char_to_int = dict((c, i) for i, c in enumerate(chars))
# summarize the loaded data
n_chars = len(raw_text)
n_vocab = len(chars)
print(f"Total Characters: {n_chars}")
print(f"Total Vocab: {n_vocab}")

# %%
# prepare the dataset of input to output pairs encoded as integers
seq_length = 100
dataX = []
dataY = []
for i in range(0, n_chars - seq_length, 1):
    seq_in = raw_text[i:i + seq_length]
    seq_out = raw_text[i + seq_length]
    dataX.append([char_to_int[char] for char in seq_in])
    dataY.append(char_to_int[seq_out])
n_patterns = len(dataX)
print(f"Total Patterns: {n_patterns}")
# reshape X to be [samples, time steps, features]
X = numpy.reshape(dataX, (n_patterns, seq_length, 1))
# normalize
X = X / float(n_vocab)
# one hot encode the output variable
y = np_utils.to_categorical(dataY)
# define the LSTM model
model = Sequential()
model.add(LSTM(256, input_shape=(X.shape[1], X.shape[2])))
model.add(Dropout(0.2))
model.add(Dense(y.shape[1], activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam')
# define the checkpoint
filepath = "weights-improvement-{epoch:02d}-{loss:.4f}.hdf5"
checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
callbacks_list = [checkpoint]
# fit the model
model.fit(X, y, epochs=20, batch_size=128, callbacks=callbacks_list)
